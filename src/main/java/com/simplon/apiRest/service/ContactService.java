package com.simplon.apiRest.service;

import com.simplon.apiRest.model.Contact;
import com.simplon.apiRest.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ContactService {
    @Autowired
    private ContactRepository contactRepository;

    public Iterable<Contact> getContact() {
        return contactRepository.findAll();
    }

    public Optional<Contact> getContact(Long id) {
        return contactRepository.findById(id);
    }

    public Contact saveContact(Contact contact) {
        return contactRepository.save(contact);
    }

    public void deleteContact(Long id){
        contactRepository.deleteById(id);
    }
}
