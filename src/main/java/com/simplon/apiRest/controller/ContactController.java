package com.simplon.apiRest.controller;

import com.simplon.apiRest.model.Contact;
import com.simplon.apiRest.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class ContactController {

    @Autowired
    private ContactService contactService;
    /**
     * Create add a new contact
     * an object contact
     * return the contact object saved
     * */
    @PostMapping("/contact")
    public ResponseEntity<Contact> createContact(@RequestBody Contact contact) {
        return ResponseEntity.status(HttpStatus.CREATED).body(contactService.saveContact(contact));
    }

    /**
     * Get all contacts
     * return - An Iterable object of Contact
     * */
    @GetMapping("/contacts")
    public Iterable<Contact> getContact() {
        return contactService.getContact();
    }

    /**
     * Get one employee
     * param id - the id of the contact
     * return A Contact object
     * */
    @GetMapping("/contact/{id}")
    public Contact getContact(@PathVariable Long id) {
        Optional<Contact> contact = contactService.getContact(id);
        if(contact.isPresent()) {
            return contact.get();
        }
        return null;
    }
    /**
     * Update - Update an existing contact
     * param id - The id of the contact to update
     * param contact - the contact object updated
     * return - the contact updated and saved
     * */
    @PutMapping("/contact/{id}")
    public ResponseEntity<Contact> updateContact(@PathVariable Long id,
                                                 @RequestBody Contact contact) {
        Optional<Contact> c = contactService.getContact(id);
        if(c.isPresent()) {
            Contact currentContact = c.get();

            String firstName = contact.getFirstName();
            if(firstName != null) {
                currentContact.setFirstName(firstName);
            }
            String lastName = contact.getLastName();
            if(lastName != null) {
                currentContact.setLastName(lastName);
            }
            Integer age = contact.getAge();
            if(age != null) {
                currentContact.setAge(age);
            }
            contactService.saveContact(currentContact);
            return ResponseEntity.status(HttpStatus.CREATED).body(currentContact);
        }
        return null;
    }

    /**
     * Delete - Delete a contact
     * param id - The id of the contact to delete
     */
    @DeleteMapping("/contact/{id}")
    public void deleteContact(@PathVariable Long id) {
        contactService.deleteContact(id);
    }
}
